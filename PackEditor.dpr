program PackEditor;

{%ToDo 'PackEditor.todo'}

uses
  Forms,
  frmEditorUnit in 'frmEditorUnit.pas' {frmEditor},
  colors in 'colors.pas',
  langload in 'langload.pas',
  frmWizardUnit in 'frmWizardUnit.pas' {frmWizard},
  common in 'common.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'W4MapPackEditor';
  Application.CreateForm(TfrmEditor, frmEditor);
  Application.Run;
end.
