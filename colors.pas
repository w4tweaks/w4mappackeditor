unit colors;
interface
uses Graphics;
type
  UColor=TColor;
const
  COLORSTYLE=1;

  {$IF (COLORSTYLE=1)}
  ucBackground:UColor = $00E3DFE0;    //224 223 227
  ucMenubar:UColor = $00EBE2E0;       //224 226 235
  ucMenuhighlight:UColor = $00C7B7BB; //187 183 199
  ucWindow:UColor = $00FFFFFF;        //255 255 255
  ucWindowText:UColor = $00000000;    //0   0   0
  {$ELSE}
  ucBackground:UColor = clRed;
  ucMenubar:UColor = clGreen;
  ucMenuhighlight:UColor = clBlue;
  ucWindow:UColor = clMaroon;
  ucWindowText:UColor = clPurple;
  {$IFEND}


implementation
end.
