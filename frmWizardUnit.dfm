object frmWizard: TfrmWizard
  Left = 779
  Top = 201
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'frmWizard'
  ClientHeight = 355
  ClientWidth = 493
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 493
    Height = 57
    Align = alTop
    Color = clWhite
    TabOrder = 1
    object lblStepName: TLabel
      Left = 16
      Top = 8
      Width = 72
      Height = 13
      Caption = 'lblStepName'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblStepDescription: TLabel
      Left = 40
      Top = 32
      Width = 85
      Height = 13
      Caption = 'lblStepDescription'
    end
  end
  object pnlButtons: TPanel
    Left = 0
    Top = 314
    Width = 493
    Height = 41
    Align = alBottom
    TabOrder = 0
    object cmdFinish: TButton
      Left = 392
      Top = 10
      Width = 89
      Height = 20
      Caption = 'cmdFinish'
      TabOrder = 1
      OnClick = cmdFinishClick
    end
    object cmdNext: TButton
      Left = 288
      Top = 10
      Width = 89
      Height = 20
      Caption = 'cmdNext'
      TabOrder = 0
      OnClick = cmdNextClick
    end
    object cmdBack: TButton
      Left = 192
      Top = 10
      Width = 89
      Height = 20
      Caption = 'cmdBack'
      Enabled = False
      TabOrder = 2
      OnClick = cmdBackClick
    end
    object cmdCancel: TButton
      Left = 16
      Top = 10
      Width = 89
      Height = 20
      Caption = 'cmdCancel'
      TabOrder = 3
      OnClick = cmdCancelClick
    end
  end
  object pnlSteps: TPanel
    Left = 0
    Top = 57
    Width = 493
    Height = 257
    Align = alClient
    TabOrder = 2
    object pnlStep2: TPanel
      Left = 1
      Top = 1
      Width = 491
      Height = 255
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object lblLanguages: TLabel
        Left = 40
        Top = 16
        Width = 75
        Height = 13
        Caption = 'lblPackName'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lstLanguages: TCheckListBox
        Left = 40
        Top = 40
        Width = 337
        Height = 201
        ItemHeight = 13
        TabOrder = 0
      end
      object cmdSelectAll: TButton
        Left = 392
        Top = 42
        Width = 89
        Height = 20
        Caption = 'cmdSelectAll'
        TabOrder = 1
        OnClick = cmdSelectAllClick
      end
      object cmdDeselectAll: TButton
        Left = 392
        Top = 66
        Width = 89
        Height = 20
        Caption = 'cmdDeselectAll'
        TabOrder = 2
        OnClick = cmdDeselectAllClick
      end
    end
    object pnlStep1: TPanel
      Left = 1
      Top = 1
      Width = 491
      Height = 255
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object lblPackName: TLabel
        Left = 40
        Top = 16
        Width = 75
        Height = 13
        Caption = 'lblPackName'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblAuthor: TLabel
        Left = 40
        Top = 112
        Width = 41
        Height = 13
        Caption = 'lblAuthor'
      end
      object lblWebSite: TLabel
        Left = 40
        Top = 160
        Width = 51
        Height = 13
        Caption = 'lblWebSite'
      end
      object lblVersion: TLabel
        Left = 40
        Top = 64
        Width = 45
        Height = 13
        Caption = 'lblVersion'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object txtPackName: TEdit
        Left = 40
        Top = 32
        Width = 377
        Height = 21
        TabOrder = 0
      end
      object txtAuthor: TEdit
        Left = 40
        Top = 128
        Width = 377
        Height = 21
        TabOrder = 2
      end
      object txtWebSite: TEdit
        Left = 40
        Top = 176
        Width = 377
        Height = 21
        TabOrder = 3
      end
      object txtVersion: TEdit
        Left = 40
        Top = 80
        Width = 377
        Height = 21
        TabOrder = 1
      end
    end
  end
end
