unit frmWizardUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, CheckLst, common, langload, strutils, colors;

type
  TfrmWizard = class(TForm)
    pnlTop: TPanel;
    pnlButtons: TPanel;
    cmdFinish: TButton;
    cmdNext: TButton;
    cmdBack: TButton;
    cmdCancel: TButton;
    lblStepName: TLabel;
    lblStepDescription: TLabel;
    pnlSteps: TPanel;
    pnlStep1: TPanel;
    lblPackName: TLabel;
    lblAuthor: TLabel;
    lblWebSite: TLabel;
    lblVersion: TLabel;
    txtPackName: TEdit;
    txtAuthor: TEdit;
    txtWebSite: TEdit;
    txtVersion: TEdit;
    pnlStep2: TPanel;
    lblLanguages: TLabel;
    lstLanguages: TCheckListBox;
    cmdSelectAll: TButton;
    cmdDeselectAll: TButton;
    procedure FormCreate(Sender: TObject);
    procedure cmdNextClick(Sender: TObject);
    procedure cmdBackClick(Sender: TObject);
    procedure cmdFinishClick(Sender: TObject);
    procedure cmdCancelClick(Sender: TObject);
    procedure cmdSelectAllClick(Sender: TObject);
    procedure cmdDeselectAllClick(Sender: TObject);
    procedure SetColors();
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function ShowWizard():boolean;

implementation

var
  ok:boolean;

{$R *.dfm}

//�������� ���� �������
function ShowWizard():boolean;
var
  f:TfrmWizard;
begin
  ok:=false;
  f:=TfrmWizard.Create(nil);
  f.ShowModal;
  if not WizardShown then
    WizardShown:=ok;
  result:=ok;
end;

//�������������
procedure TfrmWizard.FormCreate(Sender: TObject);
var
  SearchRec: TSearchRec;
  s:string;
begin
  Caption:=Translate('[Wizard.Caption]');
  cmdNext.Caption:=Translate('[Wizard.cmdNext.Caption]');
  cmdBack.Caption:=Translate('[Wizard.cmdBack.Caption]');
  cmdFinish.Caption:=Translate('[Wizard.cmdFinish.Caption]');
  cmdCancel.Caption:=Translate('[Wizard.cmdCancel.Caption]');

  lblStepName.Caption:=Translate('[Wizard.Step1.Name]');
  lblStepDescription.Caption:=Translate('[Wizard.Step1.Description]');
  lblAuthor.Caption:=Translate('[Wizard.Step1.lblAuthor.Caption]');
  lblWebSite.Caption:=Translate('[Wizard.Step1.lblWebSite.Caption]');
  lblVersion.Caption:=Translate('[Wizard.Step1.lblVersion.Caption]');
  lblPackName.Caption:=Translate('[Wizard.Step1.lblPackName.Caption]');

  txtAuthor.Text:=PackAuthor;
  txtWebSite.Text:=PackWebSite;
  txtVersion.Text:=PackVersion;
  txtPackName.Text:=PackName;

  lblLanguages.Caption:=Translate('[Wizard.Step2.lblLanguages.Caption]');
  cmdSelectAll.Caption:=Translate('[Wizard.Step2.cmdSelectAll.Caption]');
  cmdDeselectAll.Caption:=Translate('[Wizard.Step2.cmdDeselectAll.Caption]');

  if FindFirst(AppDir+'iscc\languages\*.isl', faAnyFile, SearchRec)=0 then
  begin
    s:=SearchRec.Name;
    s:=LeftStr(s,length(s)-length(ExtractFileExt(s)));
    if Languages.IndexOf(lowercase(s))>-1 then
      lstLanguages.Checked[lstLanguages.Items.Add(s)]:=true
    else
      lstLanguages.Items.Add(s);
    while FindNext(SearchRec)=0 do
    begin
      s:=SearchRec.Name;
      s:=LeftStr(s,length(s)-length(ExtractFileExt(s)));
      if Languages.IndexOf(lowercase(s))>-1 then
        lstLanguages.Checked[lstLanguages.Items.Add(s)]:=true
      else
        lstLanguages.Items.Add(s);
    end;
  end;
  FindClose(SearchRec);
  SetColors;
end;

//2-�� �������� �������
procedure TfrmWizard.cmdNextClick(Sender: TObject);
begin
  pnlStep2.Visible:=true;
  pnlStep1.Visible:=false;
  cmdNext.Enabled:=false;
  cmdBack.Enabled:=true;
  lblStepName.Caption:=Translate('[Wizard.Step2.Name]');
  lblStepDescription.Caption:=Translate('[Wizard.Step2.Description]');
end;

//1-�� �������� �������
procedure TfrmWizard.cmdBackClick(Sender: TObject);
begin
  pnlStep1.Visible:=true;
  pnlStep2.Visible:=false;
  cmdBack.Enabled:=false;
  cmdNext.Enabled:=true;
  lblStepName.Caption:=Translate('[Wizard.Step1.Name]');
  lblStepDescription.Caption:=Translate('[Wizard.Step1.Description]');
end;

//��������� ��������� �������
procedure TfrmWizard.cmdFinishClick(Sender: TObject);
var
  i:integer;
begin
  ok:=true;
  PackAuthor:=txtAuthor.Text;
  PackWebSite:=txtWebSite.Text;
  PackVersion:=txtVersion.Text;
  PackName:=txtPackName.Text;
  Languages.Clear;
  for i:=0 to lstLanguages.Count-1 do
    if lstLanguages.Checked[i] then
      Languages.Add(LowerCase(lstLanguages.Items.Strings[i]));
  Close;
end;

//��������� ������
procedure TfrmWizard.SetColors();
begin
  lblStepName.Color:=ucBackground;
  lblStepDescription.Color:=ucBackground;
  lblPackName.Color:=ucBackground;
  lblAuthor.Color:=ucBackground;
  lblWebSite.Color:=ucBackground;
  lblVersion.Color:=ucBackground;
  lblLanguages.Color:=ucBackground;

  pnlButtons.Color:=ucBackground;
  pnlSteps.Color:=ucBackground;
  pnlStep1.Color:=ucBackground;
  pnlStep2.Color:=ucBackground;

  txtPackName.Font.Color:=ucWindowText;
  txtAuthor.Font.Color:=ucWindowText;
  txtWebSite.Font.Color:=ucWindowText;
  txtVersion.Font.Color:=ucWindowText;

  lblPackName.Font.Color:=ucWindowText;
  lblAuthor.Font.Color:=ucWindowText;
  lblWebSite.Font.Color:=ucWindowText;
  lblVersion.Font.Color:=ucWindowText;
  lblStepName.Font.Color:=ucWindowText;
  lblStepDescription.Font.Color:=ucWindowText;
  lblLanguages.Font.Color:=ucWindowText;
  
  cmdFinish.Font.Color:=ucWindowText;
  cmdNext.Font.Color:=ucWindowText;
  cmdBack.Font.Color:=ucWindowText;
  cmdCancel.Font.Color:=ucWindowText;
  cmdSelectAll.Font.Color:=ucWindowText; 
  cmdDeselectAll.Font.Color:=ucWindowText;

  txtPackName.Color:=ucWindow;
  txtAuthor.Color:=ucWindow;
  txtWebSite.Color:=ucWindow;
  txtVersion.Color:=ucWindow;

  lstLanguages.Color:=ucWindow;
  pnlTop.Color:=ucBackground;
end;

//������
procedure TfrmWizard.cmdCancelClick(Sender: TObject);
begin
  Close;
end;

//������� ��� �����
procedure TfrmWizard.cmdSelectAllClick(Sender: TObject);
var
  i:integer;
begin
  for i:=0 to lstLanguages.Count-1 do
    lstLanguages.Checked[i]:=true;
end;

procedure TfrmWizard.cmdDeselectAllClick(Sender: TObject);
var
  i:integer;
begin
  for i:=0 to lstLanguages.Count-1 do
    lstLanguages.Checked[i]:=false;
end;

end.
