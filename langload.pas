unit langload;

interface
  uses Classes,Registry,Windows,Forms,SysUtils;
  procedure InitLang();                    //������� ��������� �� ������������ � ������� ����� �����
  procedure LoadLanguage(FileName:string); //�������� ����� �� ����� � ������ LanguageTranslate
  function Translate(key:String):string;   //������� �� �����(���� ������� �� ������, �� ���������� �������)
implementation

const
  CountOfWordsInLanguage=115;                                     //���������� ���� � �����
var
  LangArr:TStringList;                                            //����� ��������
  LanguageTranslate:array[0..CountOfWordsInLanguage-1] of string; //��������
  LanguageEnglish:array[0..CountOfWordsInLanguage-1] of string=   //���������� ������
  (
  'Map pack editor for Worms 4 Mayhem v.1.0 by Gerich',
  'Map pack editor for Worms 4 Mayhem v.1.0 by Gerich - /p',       //  /p=��� ����
  'Map pack editor for Worms 4 Mayhem v.1.0 by Gerich - /p (/f)',  //  /p=��� ���� , /f=��� �����

  'Started building map pack',
  'Done',
  'Failed to launch InnoSetup compiler!',
  'Error occured!',

  'New key',
  'Input name of new key',
  '[newkey]',
  'Key /k already exists',

  'New key',
  'Input name of new key',
  '[newkey]',
  'Key /k already exists',

  'New link to info file',
  'Input name of new key',
  '[newkey]',
  'Key /k already exists',


  'New folder',
  'Input name of new folder',
  'New folder',
  'Folder /f already exists',

  'Rename file',
  'Input new name',  
  'File /f already exists.',

  'File /f already exists.'+char(13)+char(10)+'Do you want to replace it?',

  'Error!',
  'Help file W4MapPackEditor.chm not found!',

  'Select files to add',
  'All(*.*)|*.*|Maps(*.xan)|*.xan|Height maps(*.hmp)|*.hmp|Tga images(*.tga)|*.tga|Lua scripts(*.lua)|*.lua|Texture lists(*.txt)|*.txt',
  '1',

  'Create setup package',
  'Map pack(*.exe)|*.exe',
  '1',

  'Save map pack project',
  'Map pack project(*.mpr)|*.mpr',
  '1',
  
  'Open map pack project',
  'Map pack project(*.mpr)|*.mpr',
  '1',

  'Save console to file',
  'Text(*.txt)|*.txt|All(*.*)|*.*',
  '1',
  'txt',

  'Select directory with files',

  'Select textures directory(Ex. /data/Themes/)',

  'Key',
  'Value',

  'File',
  'Path',

  'Maps',
  'Properties',
  'Pack properties',
  'Files',
  'Resources',
  'Preview',
  'No preview',
  'Compile status',

  'Files',
  'Maps',
  'Height maps',
  'Level icons',
  'Texture files',
  'Lua scripts',

  'File',
  'Edit',
  'Edit',
  'Edit',
  'Edit',
  'Edit',

  'New',
  'Open...',
  'Save project...',
  'Save map pack...',
  'Project options...',
  'Show console',
  'Help',
  'Exit',

  'Add',
  'Delete',

  'Add...',
  'Add link to info file...',
  'Delete',

  'Add selected resources',
  'Create folder...',
  'Delete',

  'Add...',
  'Delete',
  'Select textures directory...',

  'Add...',
  'Add folder...',
  'Add folder with subfolders...',
  'Rename...',
  'Delete',
  'Clear',

  'Save to file',
  'Clear',

  'Map pack properties',
  'Next',
  'Back',
  'Finish',
  'Cancel',

  'Map pack information',
  'Please specify some basic information about your map pack',
  'Name of pack',
  'Version',
  'Author',
  'Web site',

  'Setup languages',
  'Please specify languages which setup languages should be included',
  'Languages',
  'Select all',
  'Deselect all',
  
  '0'
  );

procedure InitLangKeys();              //��������� ������ ���� ������ � ������
begin
  LangArr.Clear;
  with LangArr do
  begin
    Add('[frmEditor.Caption]');
    Add('[frmEditor.ProjectCaption]');
    Add('[frmEditor.ProjectFullCaption]');
                                      
    Add('[message.Compile.Start]');
    Add('[message.Compile.Success]');
    Add('[message.Compile.Fail]');
    Add('[message.Compile.Error]');

    Add('[message.NewKey.Caption]');
    Add('[message.NewKey.Promt]');
    Add('[message.NewKey.Default]');
    Add('[message.NewKey.Exist]');
    
    Add('[message.NewHeaderKey.Caption]');
    Add('[message.NewHeaderKey.Promt]');
    Add('[message.NewHeaderKey.Default]');
    Add('[message.NewHeaderKey.Exist]');

    Add('[message.NewHeaderLinkKey.Caption]');
    Add('[message.NewHeaderLinkKey.Promt]');
    Add('[message.NewHeaderLinkKey.Default]');
    Add('[message.NewHeaderLinkKey.Exist]');

    Add('[message.NewFolder.Caption]');
    Add('[message.NewFolder.Promt]');
    Add('[message.NewFolder.Default]');  
    Add('[message.NewFolder.Exist]');
                                     
    Add('[message.Rename.Caption]');
    Add('[message.Rename.Promt]');
    Add('[message.Rename.Exist]');
    
    Add('[message.ReplaceFile.Caption]');

    Add('[message.HelpError.Caption]');
    Add('[message.HelpError.Promt]');

    Add('[dlgResources.Title]');
    Add('[dlgResources.Filter]');
    Add('[dlgResources.FilterIndex]');

    Add('[dlgSaveMapPack.Title]');
    Add('[dlgSaveMapPack.Filter]');
    Add('[dlgSaveMapPack.FilterIndex]');

    Add('[dlgSaveProject.Title]');
    Add('[dlgSaveProject.Filter]');
    Add('[dlgSaveProject.FilterIndex]');
    
    Add('[dlgOpenProject.Title]');
    Add('[dlgOpenProject.Filter]');
    Add('[dlgOpenProject.FilterIndex]');
    
    Add('[dlgCompile.Title]');
    Add('[dlgCompile.Filter]');
    Add('[dlgCompile.FilterIndex]');
    Add('[dlgCompile.DefaultExt]');

    Add('[dlgResourcesDir.Title]');

    Add('[dlgTexturesDir.Title]');

    Add('[TValueListEditor.Key.Name]');
    Add('[TValueListEditor.Value.Name]');
    
    Add('[lstResources.Column1.Name]');
    Add('[lstResources.Column2.Name]');

    Add('[lblMaps.Caption]');
    Add('[lblProperties.Caption]');
    Add('[lblHeader.Caption]');
    Add('[lblFiles.Caption]');
    Add('[lblResources.Caption]');
    Add('[lblPreview.Caption]');
    Add('[lblNoPreview.Caption]');
    Add('[lblCompile.Caption]');

    Add('[lstFiles.RootNames.Files]'); 
    Add('[lstFiles.RootNames.Maps]');
    Add('[lstFiles.RootNames.HeightMaps]');
    Add('[lstFiles.RootNames.LevelIcons]');
    Add('[lstFiles.RootNames.TextureFiles]');
    Add('[lstFiles.RootNames.Scripts]');

    Add('[mnuFile.Caption]');
    Add('[mnuEdit.Caption]');
    Add('[mnuHeaderEdit.Caption]');
    Add('[mnuFilesEdit.Caption]');
    Add('[mnuResourcesEdit.Caption]');
    Add('[mnuPropertiesEdit.Caption]');

    Add('[mnuFile.New.Caption]');
    Add('[mnuFile.Open.Caption]');
    Add('[mnuFile.SaveProject.Caption]');
    Add('[mnuFile.SaveMapPack.Caption]'); 
    Add('[mnuFile.PackProperties.Caption]');
    Add('[mnuFile.ShowConsole.Caption]');
    Add('[mnuFile.Help.Caption]');
    Add('[mnuFile.Exit.Caption]');

    Add('[mnuEdit.Add.Caption]');
    Add('[mnuEdit.Delete.Caption]');

    Add('[mnuHeaderEdit.Add.Caption]');
    Add('[mnuHeaderEdit.AddInfo.Caption]');
    Add('[mnuHeaderEdit.Delete.Caption]');

    Add('[mnuFilesEdit.Add.Caption]');  
    Add('[mnuFilesEdit.CreateFolder.Caption]');
    Add('[mnuFilesEdit.Delete.Caption]');

    Add('[mnuPropertiesEdit.Add.Caption]');
    Add('[mnuPropertiesEdit.Delete.Caption]');
    Add('[mnuPropertiesEdit.TexturesDir.Caption]');

    Add('[mnuResourcesEdit.Add.Caption]');
    Add('[mnuResourcesEdit.AddDir.Caption]'); 
    Add('[mnuResourcesEdit.AddSubDirs.Caption]'); 
    Add('[mnuResourcesEdit.Rename.Caption]');
    Add('[mnuResourcesEdit.Delete.Caption]');   
    Add('[mnuResourcesEdit.Clear.Caption]');

    Add('[mnuCompile.SaveToFile.Caption]');
    Add('[mnuCompile.Clear.Caption]');

    Add('[Wizard.Caption]');
    Add('[Wizard.cmdNext.Caption]');
    Add('[Wizard.cmdBack.Caption]');
    Add('[Wizard.cmdFinish.Caption]');
    Add('[Wizard.cmdCancel.Caption]');

    Add('[Wizard.Step1.Name]');
    Add('[Wizard.Step1.Description]');
    Add('[Wizard.Step1.lblPackName.Caption]');
    Add('[Wizard.Step1.lblVersion.Caption]');
    Add('[Wizard.Step1.lblAuthor.Caption]');
    Add('[Wizard.Step1.lblWebSite.Caption]');

    Add('[Wizard.Step2.Name]');
    Add('[Wizard.Step2.Description]');
    Add('[Wizard.Step2.lblLanguages.Caption]');
    Add('[Wizard.Step2.cmdSelectAll.Caption]');
    Add('[Wizard.Step2.cmdDeselectAll.Caption]');
    
    Add('[mnuFile.Help.Visible]');
  end;
end;

procedure LoadLanguage(FileName:string);    //�������� ����� �� ����� � ������ LanguageTranslate
var
  F: TextFile;
  S,S1,S2: string;
  i:integer;
begin
  try
    AssignFile(F, FileName);
    Reset(F);
    while not eof(F) do
    begin
      Readln(F, S);
      s2:='';
      s:=s+' ';
      i:=1;
      while (i<Length(s)) do
      begin
        if (S[i]='/') then
        begin
          if (s[i+1]='=') then
            s2:=s2+'='                      // ������ �����
          else
            if (s[i+1]='n') then
              s2:=s2+char(13)+char(10)      // ������� ������
            else
              if (s[i+1]='/') then
                s2:=s2+'/';                 // ����
          inc(i);
        end
        else
          if (s[i]='=') then
          begin
            s1:=s2;
            s2:='';
          end
          else
            s2:=s2+s[i];
        inc(i);
      end;
      i:=LangArr.IndexOf(S1);
      if i<>-1 then
        LanguageTranslate[i]:=s2;
    end;
    CloseFile(F);
  finally

  end;
end;

function Translate(key:String):string; //������� �� �����(���� ������� �� ������, �� ���������� �������)
var
  p:integer;
begin
  p:=LangArr.IndexOf(Key);
  if(p<0) then
    result:='ERROR'
  else
    result:=LanguageTranslate[p];
end;

procedure InitLang();       //������� ��������� �� ������������ � ������� ����� �����
var
  i:integer;
  LangFile:string;
begin
  for i:=0 to CountOfWordsInLanguage-1 do
    LanguageTranslate[i]:=LanguageEnglish[i];
  LangFile:='Lang.lng';            //������� ��������� ���������������� ������
  if FileExists(ExtractFilePath(application.ExeName)+ LangFile) then
    LoadLanguage(LangFile)
end;

begin
  LangArr:=TStringList.Create;
  InitLangKeys;
  InitLang;
end.

