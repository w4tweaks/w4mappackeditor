unit maplistload;

interface
  uses Classes,Registry,Windows,Forms,SysUtils,common;
  function GetKey(maplistfile,key:String):string;   //������� �� �����(���� ������� �� ������, �� ���������� �������)
  function MakeScripts(Header,Body,AdditionalMaps,XmlEnd,DatabankBegin:TStrings;maplistfile, SaveTo,textKey:string):string;   //�������� ����� Scripts.xml
implementation

uses StrUtils;

function GetKey(maplistfile,key:String):string; //�������� �����(���� ���� �� ������, �� ERROR)
var
  f:TStrings;
  S,S1,S2: string;
  i,j:integer;
label
  l1,l2;
begin
  f:=TStringList.Create() ;
  f.LoadFromFile(maplistfile);
  for j:=0 to f.Count-1 do
  begin
    if f.Strings[j][1]='{' then
      goto l1;
    s2:='';
    s:=f.Strings[j]+' ';
    i:=1;
    while (i<Length(s)) do
    begin
      if (S[i]='/') then
      begin
        if (s[i+1]='=') then
          s2:=s2+'='                      // ������ �����
        else
          if (s[i+1]='n') then
            s2:=s2+char(13)+char(10)      // ������� ������
          else
            if (s[i+1]='/') then
              s2:=s2+'/';                 // ����
        inc(i);
      end
      else
        if (s[i]='=') then
        begin
          s1:=s2;
          s2:='';
        end
        else
          s2:=s2+s[i];
      inc(i);
    end;
    if s1=key then
    begin
      result:=s2;
      goto l2;
    end;
  end;
  l1:
  result:='ERROR';
  l2:
end;

function MakeScripts(Header,Body,AdditionalMaps,XmlEnd,DatabankBegin:TStrings;maplistfile, SaveTo,textKey:string):string;   //�������� ����� Scripts.xml
var
  Scripts,BodyOfScrips,copyAdditionalMaps,f,UnicodeFile,DatabankFile:TStrings;
  i,j,k,m,Index,bracket:integer;
  sUnicodeText:string;
  s,sText,sImage,sScripts,sMap,sTextures,sTexture1,sTexture2:string;
begin
  Scripts:=TStringList.Create();
  Scripts.AddStrings(Header);
  BodyOfScrips:=TStringList.Create();
  BodyOfScrips.AddStrings(Body);
  copyAdditionalMaps:=TStringList.Create();
  copyAdditionalMaps.AddStrings(AdditionalMaps); 
  DatabankFile:=TStringList.Create();

  f:=TStringList.Create() ;
  f.LoadFromFile(maplistfile);

  UnicodeFile:=TStringList.Create();
  if FileExists(MapLists+textKey+'.txt') then
    UnicodeFile.LoadFromFile(MapLists+textKey+'.txt');
  j:=0;
  bracket:=0; //��� ������� {}
  for i:=0 to f.Count-1 do
  begin
    if bracket=1 then
    begin
      if PosEx(textKey+'=',f.Strings[i])=1 then
      begin                   //�������� ����� �����(�������������� � ������)
        sText:=RightStr(f.Strings[i],length(f.Strings[i])-length(textKey)-1);
        sUnicodeText:='';
        for k:=1 to length(sText) do
        begin
          Index:=-1;
          for m:=0 to (UnicodeFile.Count div 2)-1 do
            if UnicodeFile.Strings[m*2] = sText[k] then
              Index:=m*2+1;
          if Index=-1 then
            sUnicodeText:=sUnicodeText+'&#x00' + IntToHex( integer(sText[k]),2) + ';'
          else
            sUnicodeText:=sUnicodeText+'&#x'+UnicodeFile.Strings [Index] + ';';
        end
      end
      else
        if PosEx('[image]=',f.Strings[i])=1 then
          sImage:=RightStr(f.Strings[i],length(f.Strings[i])-8)
        else
          if PosEx('[scripts]=',f.Strings[i])=1 then
            sScripts:=RightStr(f.Strings[i],length(f.Strings[i])-10)
          else
            if PosEx('[map]=',f.Strings[i])=1 then
              sMap:=RightStr(f.Strings[i],length(f.Strings[i])-6)
            else
              if (PosEx('[text]=',f.Strings[i])=1) and (textKey<> '') then
                sUnicodeText:=RightStr(f.Strings[i],length(f.Strings[i])-7)
              else
                if PosEx('[textures]=',f.Strings[i])=1 then
                  sTextures:=RightStr(f.Strings[i],length(f.Strings[i])-11)
                else
                  if PosEx('[texture1]=',f.Strings[i])=1 then
                    sTexture1:=RightStr(f.Strings[i],length(f.Strings[i])-11)
                  else
                    if PosEx('[texture2]=',f.Strings[i])=1 then
                      sTexture2:=RightStr(f.Strings[i],length(f.Strings[i])-11);
      if f.Strings[i][1]='}' then
      begin
        bracket:=0;
        //������ ��������
        DatabankFile.Clear;
        DatabankFile.AddStrings(DatabankBegin);
        DatabankFile.Add('    <XStringResourceDetails id=''Databank.MaterialFile''>');
        DatabankFile.Add('      <Value>'+sTextures+'</Value>');
        DatabankFile.Add('      <Name>Databank.MaterialFile</Name>');
        DatabankFile.Add('      <Flags>64</Flags>');
        DatabankFile.Add('    </XStringResourceDetails>');
        DatabankFile.Add('    <XStringResourceDetails id=''Databank.Theme''>');
        DatabankFile.Add('      <Value>BUILDING</Value>');
        DatabankFile.Add('      <Name>Databank.Theme</Name>');
        DatabankFile.Add('      <Flags>64</Flags>');
        DatabankFile.Add('    </XStringResourceDetails>');
        DatabankFile.Add('    <XStringResourceDetails id=''Databank.TimeOfDay''>');
        DatabankFile.Add('      <Value>NIGHT</Value>');
        DatabankFile.Add('      <Flags>64</Flags>');
        DatabankFile.Add('    </XStringResourceDetails>');
        DatabankFile.Add('    <XStringResourceDetails id=''Heightmap.BaseTexture''>');
        DatabankFile.Add('      <Value>'+sTexture1+'</Value>');
        DatabankFile.Add('      <Name>Heightmap.BaseTexture</Name>');
        DatabankFile.Add('      <Flags>64</Flags>');
        DatabankFile.Add('    </XStringResourceDetails>');
        DatabankFile.Add('    <XStringResourceDetails id=''Heightmap.SecondTexture''>');
        DatabankFile.Add('      <Value>'+ sTexture2  +'</Value>');
        DatabankFile.Add('      <Name>Heightmap.SecondTexture</Name>');
        DatabankFile.Add('      <Flags>64</Flags>');
        DatabankFile.Add('    </XStringResourceDetails>');
        DatabankFile.Add('  </xomObjects>');
        DatabankFile.Add('</xomArchive>');
        DatabankFile.SaveToFile(WormsDir+'data\databanks\'+sMap+'.xml');

        //����� ���������� � ����� � Scripts.xml
        if AdditionalMaps.Count >0 then
        begin
          s:=copyAdditionalMaps.Strings[0];
          copyAdditionalMaps.Delete(0);
          Scripts.Add     ( '      <ContainerResources href=''' + s + '''/>');
          Scripts.Add     ( '      <StringResources href=''txt.'+ s + '''/>'); 
          BodyOfScrips.Add('    ');
          BodyOfScrips.Add('    <XContainerResourceDetails id='''+s+'''>');
          BodyOfScrips.Add('      <Value href=''' + s + '-0''/>');
          BodyOfScrips.Add('      <Name>' + s + '</Name>');
          BodyOfScrips.Add('      <Flags>80</Flags>');
          BodyOfScrips.Add('    </XContainerResourceDetails>');
          BodyOfScrips.Add('    <WXFE_LevelDetails id=''' +s+ '-0''>');
          BodyOfScrips.Add('      <Frontend_Name>txt.'+ s +'</Frontend_Name>');
          BodyOfScrips.Add('      <Frontend_Briefing></Frontend_Briefing>');
          BodyOfScrips.Add('      <Frontend_Image>'+ sImage +'</Frontend_Image>');
          BodyOfScrips.Add('      <Level_ScriptName>'+ sScripts +'</Level_ScriptName>');
          BodyOfScrips.Add('      <Level_FileName>'+ sMap +'</Level_FileName>');
          BodyOfScrips.Add('      <Objectives></Objectives>');
          BodyOfScrips.Add('      <Level_Number>6</Level_Number>');
          BodyOfScrips.Add('      <Level_Type>0</Level_Type>');
          BodyOfScrips.Add('      <Lock></Lock>');
          BodyOfScrips.Add('      <Theme_Type>5</Theme_Type>');
          BodyOfScrips.Add('      <Preview_Type>0</Preview_Type>');
          BodyOfScrips.Add('      <BonusTime>165</BonusTime>');
          BodyOfScrips.Add('    </WXFE_LevelDetails>');
          BodyOfScrips.Add('    <XStringResourceDetails id=''txt.'+ s+ '''>');
          BodyOfScrips.Add('      <Value>' + sUnicodeText + '</Value>');
          BodyOfScrips.Add('      <Name>txt.' + s + '</Name>');
          BodyOfScrips.Add('      <Flags>64</Flags>');
          BodyOfScrips.Add('    </XStringResourceDetails>');
        end;
      end;
    end;
    if bracket=0 then
      if f.Strings[i][1]='{' then
      begin
        bracket:=1;
        sText:='('+IntToStr(j+1)+')';
        sImage:='NoLevel.tga';
        sScripts:='stdvs,wormpot';
        sMap:='Multi_DestructAndServe';
        sTextures:='ThemeCamelot\ThemeCamelot.txt';
        sTexture1:='C01';
        sTexture2:='C02';
      end;
  end;
  Scripts.AddStrings(BodyOfScrips); 
  Scripts.AddStrings(XmlEnd);
  Scripts.SaveToFile(SaveTo);
end;

end.

